connected_once = false

if io?
  socket = io()
  socket.on "connect", () -> 
    if not connected_once
      connected_once = true
    else
      window.location.reload()

wrap = (i, l) -> if i >= 0 then i % l else l + (i % l)
clamp = (a, mn, mx) -> Math.min(Math.max(a, mn), mx)

font = null
ctx = null
camera_permission = null
socket = null
main_image = null

started = false

state = new State()


on_capture_change = () ->
  window.camera_permission = null
  state.capture_ready()
  windowResized()
  ui.start.hide()
  update_ui state

post_frame_to_server = (url, frame) ->
  d = new URLSearchParams()
  for k, v of frame
    d.append k, v

  response = await fetch(url, {method: 'POST', body: d})
  return response

get_free_memory = () ->
  megabyte = (b) -> Math.floor(b / 1000000)
  heap = megabyte performance.memory.totalJSHeapSize
  max = megabyte performance.memory.jsHeapSizeLimit
  percent = 100 - Math.floor((heap / max) * 100)
  percent
  # console.log "memory : #{percent}% [#{heap} / #{max}]  --- free : #{max - heap}"
  # console.log "used ", megabyte performance.memory.usedJSHeapSize

get_time_string = () ->
  d = new Date().toLocaleString()
  date = d.split(",")[0].replaceAll "/", "-"
  t = d.split(", ")[1].split(":").slice(0,2).join("-")
  t + "_" + date

repeated = (t, c) ->
  if c is 0 then ""
  else
    [0..c-1].fill(t).join ""

zeropad = (v, max) ->
  c = (max + "").length - 1
  s = v + ""
  repeated("0", c) + s


last_zip_delta = 0
zip_deltas = []

now = () -> new Date().getTime()

average = (ds) -> Math.floor(ds.reduce((a, v, i) -> a + v) / ds.length)
print_time = (t) ->
  ms = Math.floor(t / 60)
  s = t % 60
  "#{zeropad(ms, 10)}:#{zeropad(s, 10)}"

update_zip_progress = (i) ->
  # console.log i
  a = state.frames.length
  # n = now()
  # d = Math.floor((n - last_zip_delta) * 0.001)
  # zip_deltas.push d
  # last_zip_delta = n
  # average_zip_time = average zip_deltas
  # remaining_frames = a - i - 1
  ui.downloading_text.html "zipping #{i + 1}/#{a}"
  # ui.downloading_text.html "zipping #{i + 1}/#{a}\ntime left : #{print_time(Math.floor(remaining_frames * average_zip_time))}"


update_gif_progress = (i) ->
  a = state.frames.length
  ui.downloading_text.html "giffing #{i + 1}/#{a}"

update_load_progress = (i, a) ->
  # console.log i, a
  if i is a
    ui.downloading_panel.hide()
    console.log "all frames loaded"
    state.change_res state.frames[0].image.width, state.frames[0].image.height
  else
    ui.downloading_text.html "loading #{state.title}...\n#{i}/#{a}"


load_frames = (z) ->
  console.log z
  t = z.name.split("_")
  t = t.slice(0, t.length - 2).join("_")
  state.title = t
  zipReader = new zip.ZipReader new zip.BlobReader(z)
  entries = await zipReader.getEntries();
  index = 0
  ui.downloading_text.html "loading #{state.title}..."
  ui.downloading_panel.show()
  ui.details_panel.hide()
  entries.map((e) ->
    data = await e.getData(new zip.Data64URIWriter());
    i = loadImage data, (im) ->
      state.add_frame(
        image : im
        data : data
      )
      # console.log im
      index++
      update_load_progress index, entries.length
  )
  await zipReader.close();

  ui.upload.elt.value = ""

download_blob = (name, blob) ->
  link = document.createElement("a")
  link.href = URL.createObjectURL blob
  link.download = name
  link.click()
  link.remove()

download_zip = () ->
  if state.frames.length > 0
    ui.details_panel.hide()
    title = state.get_title()
    ui.downloading_panel.show()
    files = []
    l = state.frames.length
    for frame, i in state.frames
      data = await fetch(frame.data)
      # data = frame.data
      files.push { name : "#{title}_#{zeropad(i + 1, l)}.png", input : data }
      ui.downloading_text.html "preparing #{i + 1}/#{l}"
    zip_deltas = []
    last_zip_delta = now()
    ui.downloading_text.html "starting zip..."
    blob = await zipup(files, update_zip_progress).blob()
    download_blob "#{title}_#{get_time_string()}.zip", blob
    ui.details_panel.hide()
    ui.downloading_panel.hide()
    

download_gif = () ->
  if state.frames.length > 0
    gif = new GIF(
      workers: 2,
      quality: 10
    )
    for f in state.frames
      gif.addFrame(f.image.canvas, {delay : 1000 / state.fps})

    gif.on "progress", (p) -> update_gif_progress(Math.floor(p * (state.frames.length - 1)))
    gif.on 'finished', (blob) ->
      download_blob "#{state.get_title()}.gif", blob
      ui.downloading_panel.hide()

    ui.details_panel.hide()
    ui.downloading_panel.show()
    gif.render()
    # ui.downloading_panel.hide()



haptic = (t = 3) ->
  navigator.vibrate(t)  

get_pointer = (p) ->
  x : p.x 
  y : p.y + state.scroll

collide_pointer = (lp) ->
  p = get_pointer lp
  ks = Object.keys(gui).reverse()

  for k in ks
    if gui[k].collide p
      return gui[k]
  return null

pan = (e) ->
  # console.log "pan", e
  cp = e.center

  state.pointer_delta = 
    x : cp.x - state.pointer.x
    y : cp.y - state.pointer.y

  state.pointer = 
    x : cp.x
    y : cp.y

  if not state.scroll_determined
    if abs(e.deltaY) > state.scroll_deadzone
      if state.pan_target?
        if state.pan_target.name isnt "bar"
          state.pan_target.dirty = true
          state.pan_target.panend state
          state.pan_target = gui.bar
        state.scroll_determined = true
    else if abs(e.deltaX) > state.scroll_deadzone
      # state.pan_target = gui.bar
      state.scroll_determined = true
  else
    if state.pan_target?
      state.pan_target.dirty = true
      state.pan_target.pan state

panstart = (e) ->
  # console.log  "pan start", e
  state.scroll_determined = false
  state.pointer = e.center
  state.pan_target = 
    if state.pointer.x > (innerWidth - menu_width)  then gui.bar 
    else 
      t = collide_pointer state.pointer
      t
  if state.pan_target?
    state.pan_target.dirty = true
    state.pan_target.panstart state, e
  else
    console.log "gui missing"
      
panend = (e) ->
  # console.log  "pan end", e
  if state.pan_target?
    state.pan_target.panend state, e
    state.pan_target = null
 
tap = (e) ->
  state.pointer = e.center
  d = collide_pointer state.pointer
  if d?
    # console.log "tap", d, state.pointer
    d.dirty = true
    d.tap state, e
    if d.name isnt "frame_view" or state.play
      haptic()
    # e.stopPropagation()

press = (e) ->
  # console.log "press", e
  state.pointer = e.center
  d = collide_pointer e.center
  if d?
    d.dirty = true
    d.press state
    haptic()

pressup = (e) ->

  # console.log "pressup", e
  if e.isFinal
    d = collide_pointer e.center
    if d?
      d.dirty = true
      d.pressup state

# doubletap = (e) ->
#   # console.log e
#   if preview
#     play = not play
#     dirty = true
#   # console.log e


start = () ->
  haptic()
  if isMobile.android.device
    fullscreen(true)
    screen.orientation.lock "landscape"
  # screen.orientation.onorientationchange = (e) ->
  #   console.log e
  ui.start.mousePressed false
  ui.start.html "starting camera..."
  started = true
  state.camera_handler.on_capture_change = on_capture_change
  state.camera_handler.start()

continue_app = (e) ->
  if isMobile.android.device
    fullscreen(true)
    screen.orientation.lock "landscape"
  ui.start.hide()
  state.focus()


preload = () ->
  font = loadFont('font/SpaceMono-Bold.ttf')
setup = () ->
  ctx = createCanvas(innerWidth, innerHeight);
  textFont font

  hammer = new Hammer.Manager(ctx.elt, {});
  hammer.add new Hammer.Tap(event : "twofinger", pointers : 2)
  hammer.add new Hammer.Tap(event : "singletap")
  hammer.add new Hammer.Pan()
  hammer.add new Hammer.Press(event:"press")
  hammer.add new Hammer.Press(event:"pressup")
  # hammer.add new Hammer.Tap(event : "doubletap", taps : 2)
  # hammer.on "twofinger", doubletap
  # hammer.on "doubletap", doubletap
  hammer.on 'singletap', tap
  hammer.on "pan", pan
  hammer.on "panstart", panstart
  hammer.on "panend", panend
  hammer.on "press", press
  hammer.on "pressup", pressup

  # window.screen.orientation.addEventListener "change", (e) -> console.log e

  build_menu()
  init_gui()

  navigator.mediaDevices.getUserMedia(Capturer.create_request())
    .then((stream) -> 
      camera_permission = stream
      if isMobile.apple.device
        if screen.orientation.type isnt "landscape-primary"
          ui.start.html "rotate phone to landscape"
        else
          ui.start.html "go to fullscreen to start"
      else
        ui.start.mousePressed start
        ui.start.html "tap to start"

    )
    .catch((err) -> ui.start.html("no permission to use camera!"))

  window.addEventListener "focus", (e) ->
    if started
      state.focus()

      # if not state.camera_handler.loaded
      #   state.camera_handler.start()
  window.addEventListener "blur", (e) ->
    if started
      state.defocus()

  #     if state.camera_handler.loaded
  #       state.camera_handler.stop()

  window.addEventListener "wheel", (e) -> 
    d = Math.sign(e.deltaY)
    state.scroll_view(d * (menu_height - 10))
    state.snap_scroll(- d)
    if state.scroll >= 0
      gui.settings_close_button.close_menu state, false
    else
      gui.settings_menu_button.open_menu state, false


  screen.orientation.addEventListener "change", (e) ->
    if e.currentTarget.type is "portrait-primary"
      ui.start.show()
      console.log e
      state.defocus()
      ui.start.html "pascam suspended\ntap to return"
      ui.start.mousePressed continue_app
    else
      if not started
        start()
      else
        continue_app()
    init_gui()


windowResized = () ->
  if ctx?
    resizeCanvas innerWidth, innerHeight

    previous_scroll = state.scroll / menu_height

    state.width_ratio = state.camera_handler.width / state.camera_handler.height

    state.viewport_height = if state.camera_handler.height < innerHeight then innerHeight else Math.min(state.camera_handler.height, innerHeight)
    state.viewport_width = state.viewport_height * state.width_ratio

    init_gui()

    state.scroll = previous_scroll * menu_height


draw_divs = () ->
  for k, v of gui
    v.update state
    v.render state

draw = () ->
  if state?
    if state.pan_target? and state.pan_target.name is "bar"
      state.dirty = true

    if state.loaded
      translate 0, - state.scroll
      draw_divs()
      state.update deltaTime

    # if state.camera_dirty
    #   state.camera_dirty = false
  if main_image?
    image main_image, 0, 0, innerWidth, innerHeight

### 

FIX
  wait for previous capture before capturing frame?
  onion cant set on onion - whut?

TODO

  show start gui again on portrait mode

  show key frames on length slider

  save/load last project settings

  replace buttons in upper menu

  wrapping onions

  absolute onion layer

  include live in download

  frames aspect

  pretty question panels

  cam settings
    presets

  operations
    reverse
    clone

  play modes
    wiggle 
    faded

  files
    zip naming options
      no date
  
  vertical mode
    enlarge viewport

  show captured frame?

  capture video frames?

LOCAL SERVER
  modify frame order in local
  save periodical frame backup

OPTIMIZE

###