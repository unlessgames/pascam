class CameraSetting
  constructor : (@name, @enabler, @value, @min, @max, @step) ->
    n = ""
    for i in [0..@name.length - 1]
      c = @name[i]
      n += if c is c.toUpperCase() then " #{c.toLowerCase()}" else c
    @readable_name = n
    @value = @min + @range() * 0.5
  range : () -> @max - @min
  auto : false
  clamp : (v) ->
    @value = clamp v, @min, @max
  normalized : () ->
    (@stepped() - @min) / @range()
  get_stepped : (v) ->
    if @step is 0
      v
    else
      Math.floor(v / @step) * @step
  stepped : () ->
    @get_stepped @value

  @defaults :
    focusDistance : "focusMode"
    iso : "none"
    exposureTime : "exposureMode"
    brightness : "none"
    contrast : "none"
    saturation : "none"
    sharpness : "none"
    colorTemperature : "whiteBalanceMode"
    torch : "none"


class Capturer
  settings : []
  capturing : false
  loaded : false
  capture : null
  request : null
  constraint_queue : null
  width : 1280
  height : 720

  torch : false

  on_capture_change : () ->
  constructor : () ->
    @request = Capturer.create_request()
  @create_request : (w = 1280, h = 720) ->
    audio: false
    video:
      facingMode: "environment"
      width : 
        ideal : w
      height:
        ideal : h
  stop : () ->
    # console.log "stopping capture", @capture
    if @capture?
      t = @get_track()
      if t?
        t.stop()
      else
        console.log "nothing to stop"
      @capture = null
      @loaded = false
    else
      console.log "nothing to stop"
  update : () ->
    if @dirty 
      @dirty = false
  toggle_torch : () ->
    @ensure_constraint_queue()
    @torch = not @torch
    @constraint_queue.torch = @torch
    if not @applying 
      @apply_constraints()

  ensure_constraint_queue : () ->
    if not @constraint_queue?
      @constraint_queue = {}

  set : (setting, value) ->
    last = setting.stepped()
    setting.value = value
    next = setting.stepped()

    if last isnt next
      @ensure_constraint_queue()

      @constraint_queue[setting.name] = setting.stepped()

      if setting.enabler isnt "none"
        @constraint_queue[setting.enabler] = "manual"
        setting.auto = false

      @apply_constraints()
    # console.log "setting features ", @constraint_queue
  set_auto : (setting) ->
    setting.value = setting.min + setting.range() * 0.5

    @ensure_constraint_queue()

    @constraint_queue[setting.name] = setting.stepped()

    if setting.enabler isnt "none"
      setting.auto = true
      @constraint_queue[setting.enabler] = "continuous"

    @apply_constraints()

  applied_constraints : (e) ->
    @applying = false
    if @constraint_queue?
      @apply_constraints @constraint_queue

    @on_capture_change()

  apply_constraints : () ->
    t = @get_track()
    if t?
      if not @applying
        c = @constraint_queue
        @constraint_queue = null
        @applying = true
        t.applyConstraints(advanced : [c])
          .then (e) => @applied_constraints()
          .catch (err) -> console.log err
        if t.torch? and not t.torch
          @stop()
          @start()
    else
      console.log "no track to apply settings to"

  get_track : () ->
    if @capture? and @capture.elt.srcObject?
      @capture.elt.srcObject.getVideoTracks()[0]
    else
      console.log "no video track found"

  capabilities : () -> 
    t = @get_track()
    if t? then t.getCapabilities()
    else null

  capture_ready : (e) ->
    # console.log "capture ready", e
    @width = @capture.width
    @height = @capture.height
    camera_permission = null
    @loaded = true
    # state.set_capture(window.capture_temp)
    cs = @capabilities()
    if cs?
      keys = Object.keys(cs).filter((k) -> CameraSetting.defaults[k]?)
      @settings = keys.map((k) -> new CameraSetting(k, CameraSetting.defaults[k], cs[k].min, cs[k].min, cs[k].max, cs[k].step)).filter (v) -> v.name isnt "torch"

      current_cam_settings = @get_track().getSettings()

      for k, v of current_cam_settings
        s = @settings.find((se) -> se.name is k)
        # console.log k, v
        if s?
          s.value = v
        else 
          s = @settings.find (se) -> se.enabler is k
          if s?
            s.auto = v isnt "manual"
        # TODO set auto/manual

    # @constraint_queue = {}
    # for s, i in @settings
    #   if s.enabler != "none"
    #     @constraint_queue[s.enabler] = "continuous"
    #     @settings[i].auto = true

    @capture.elt.requestVideoFrameCallback @process_camera.bind @

    # @apply_constraints()
    @applied_constraints()
  change_res : (w, h) ->
    # @stop()
    @loaded = false
    @request = Capturer.create_request w, h
    console.log @request.video.width.ideal, @request.video.height.ideal
    @start()
  start : (force = true) ->
    if not @request?
      @request = Capturer.create_request()

    # console.log "starting capture", @request

    if not @capture? or force
      delete @capture
      @capture = createCapture @request, VIDEO, @capture_ready.bind @
    else
      console.log "a capture is already being created"

  process_camera : () ->
    if @capture?
      # if not state.pan_target? or state.pan_target.name isnt "bar"
      @image = @capture.get()
      @image.is_camera = true

      @dirty = true

      state.loaded = true

      @capture.elt.requestVideoFrameCallback @process_camera.bind @

  trigger : (callback) ->
    if @capture? and not @capturing
      ctx.elt.style.filter = "invert(1)"

      setTimeout(
        () => 
          f = createImage(@width, @height)
          f.copy(@capture, 0, 0, @width, @height, 0, 0, f.width, f.height);

          frame = 
            image : f
            data : f.canvas.toDataURL()
    
          callback frame          
          ctx.elt.style.filter = "invert(0)"

        16
      )


