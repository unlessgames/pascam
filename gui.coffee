colors = 
  border : "#666"
  fg : 100
  bg : 0
  white : 200
  highlight : "#e42"

ui = 
  mode : null
  frame : null
  fps : null
  del : null
  details : null
  question_panel: null
  cancel : null
  confirm : null


cancel_question = () ->
  haptic()
  ui.question_panel.hide()

start_confirm = (text, fun) ->
  ui.question.html text
  ui.question_panel.show()
  ui.confirm.mousePressed false
  ui.confirm.mousePressed () ->
    fun()
    ui.question_panel.hide()

  ui.cancel.mousePressed false
  ui.cancel.mousePressed cancel_question

build_menu = () ->

  w = innerHeight / 6

  ui.start = createButton("waiting for camera...")
  ui.start.elt.id = "start"

  ui.question_panel = createDiv("")
  ui.question_panel.elt.id = "question_panel"

  ui.confirm = createButton("yes")
  ui.confirm.elt.id = "confirm"
  ui.cancel = createButton("noo")
  ui.cancel.elt.id = "cancel"
  ui.cancel.mousePressed cancel_question

  ui.question = createDiv("")
  ui.question.elt.id = "question"
  ui.question.parent ui.question_panel.elt
  ui.confirm.parent ui.question_panel.elt
  ui.cancel.parent ui.question_panel.elt


  ui.downloading_panel = createDiv("")
  ui.downloading_panel.elt.id = "downloading_panel"
  ui.downloading_text = createDiv("")
  ui.downloading_text.elt.id = "downloading_text"
  ui.downloading_text.html "zzz"
  ui.downloading_text.parent ui.downloading_panel.elt
  ui.downloading_panel.hide()
  


  ui.details_panel = createDiv("")
  ui.details_panel.elt.id = "details_panel"
  ui.details_panel.hide()


  ui.download = createButton("download zip")
  ui.download.parent ui.details_panel.elt
  ui.download.elt.id = "download"
  ui.download.elt.addEventListener "click", (e) ->
    download_zip()


  ui.gif = createButton("generate gif")
  ui.gif.parent ui.details_panel.elt
  ui.gif.elt.id = "gif"
  ui.gif.elt.addEventListener "click", (e) ->
    download_gif()


  ui.upload = createInput("upload", "file")
  ui.upload.elt.accept = ".zip" 
  ui.upload.parent ui.details_panel.elt
  ui.upload.elt.id = "upload"
  ui.upload.elt.addEventListener "change", (e) ->
    # console.log e
    # console.log ui.upload.elt.files[0]
    load_frames ui.upload.elt.files[0]

  ui.upload_button = document.createElement("Label")
  ui.upload_button.setAttribute "for", "upload"
  ui.upload_button.innerHTML = "open zip"
  ui.upload_button.id = "upload_button"
  ui.details_panel.elt.appendChild ui.upload_button


  ui.title = createInput("frames")
  ui.title.parent ui.details_panel.elt
  ui.title.elt.id = "title_input"
  ui.title.elt.addEventListener "change", (e) ->
    state.change_title ui.title.elt.value
    ui.details_panel.hide()

  ui.width_input = createInput(state.camera_handler.width, "number")
  ui.width_input.parent ui.details_panel.elt
  ui.width_input.elt.id = "width_input"

  ui.height_input = createInput(state.camera_handler.height, "number")
  ui.height_input.parent ui.details_panel.elt
  ui.height_input.elt.id = "height_input"

  ui.res_change = createButton("ok")
  ui.res_change.parent ui.details_panel.elt
  ui.res_change.elt.id = "submit_res"

  ui.res_change.elt.addEventListener "click", () ->
    w = ui.width_input.elt.valueAsNumber
    h = ui.height_input.elt.valueAsNumber
    state.change_res w, h

    ui.details_panel.hide()

  
  ui.details = createDiv("")
  ui.details.parent ui.details_panel.elt
  ui.details.elt.id = "details"


  ui.question_panel.size innerWidth, innerHeight
  ui.confirm.size w * 2, w
  ui.cancel.size w * 2, w



update_ui = (s) ->
  ui.height_input.elt.value = s.camera_handler.height
  ui.width_input.elt.value = s.camera_handler.width
# update_details = () ->
#   s = """
#   title : #{ui.title.elt.value}
#   resolution : #{state.capture.width} x #{state.capture.height}
#   """
#   if state.frames.length is 0
#     s += "\n\ntap viewport to capture state.frames"
#   else
#     s += """\nfps : #{fps}
#     state.frames : #{state.frames.length}
#     duration : #{((frame_delta * state.frames.length) / 1000).toFixed(2)}s
#     """


#   ui.details.html(s)

# toggle_info = () ->
#   haptic()
#   info = not info
#   if info
#     update_details()
#     ui.details_panel.show()
#   else 
#     ui.details_panel.hide()

menu_height = 0
menu_width = 0
menu_pos = 0

class Div
  constructor : (@name = "", fields) ->
    for k, v of fields
      @[k] = v
    if not @transparent
      @ctx = createGraphics(@w, @h)
    # @ctx.setAttributes("antialias", false)

  x : 0
  y : 0
  w : 50
  h : 50
  dirty : true
  visible : true
  text_color : colors.fg
  back_color : colors.bg
  border_color : colors.fg
  highlight : (a = true) ->
    @border_color = if a then colors.highlight else colors.fg
    @dirty = true
  needs_update : (s) -> false
  update : (s) -> 
    if @needs_update s
      @dirty = true
  panend : (s) ->
  panstart : (s) ->
  press : (s) ->
    @down = true
  debug : false
  show : (s = true) ->
    @visible = s
    @dirty = true
  pressup : (s) ->
    @down = false
  tap : (s) ->
    # console.log s
  pan : (s) ->
    # console.log @name, s.pointer, s.pointer_delta
  local_pointer : (s) ->
    lp = 
      x : clamp(s.pointer.x, 0, @w) / @w
      y : clamp(s.pointer.y + s.scroll - @y, 0, @h) / @h
    lp

  draw_border : (x, y, w, h, sw, c = colors.highlight) ->
    stroke c
    noFill()
    strokeWeight sw
    rect x + sw * 0.5,y + sw * 0.5,w - sw,h - sw
    strokeWeight 1

  draw_border_local : (x, y, w, h, sw, c = colors.highlight) ->
    @ctx.stroke c
    @ctx.noFill()
    @ctx.strokeWeight sw
    @ctx.rect x + sw * 0.5,y + sw * 0.5,w - sw,h - sw
    @ctx.strokeWeight 1

  clear_rect : () ->
    fill colors.bg
    noStroke()
    rect @x, @y, @w, @h
  local_clear_rect : () ->
    @ctx.fill colors.bg
    @ctx.noStroke()
    @ctx.rect 0,0, @w, @h

  border : (c, sw = 2) ->
    @draw_border_local 0,0,@w,@h, sw, c
  text : (t, x, y, c = colors.fg, h = @h * 0.5) ->

    @ctx.push()
    @ctx.noStroke()
    @ctx.textAlign CENTER, CENTER
    @ctx.textSize h
    @ctx.translate 0, -(h / @ctx.textDescent())

    @ctx.fill c
    @ctx.text t, x, y
    @ctx.pop()

  filled_text : (t, fc = colors.fg, bc = colors.bg, sc = colors.fg, sw = 3, x = 0, y = 0, w = @w, h = @h) ->
    @ctx.fill bc
    @ctx.noStroke()
    @ctx.rect x, y, w, h
    @ctx.push()
    @ctx.textAlign CENTER, CENTER
    @ctx.fill fc
    # textSize (@w / (t.length)) * 1
    s = (h) / t.length
    # console.log s, h * 0.5
    # console.log "text descent post", @ctx.textDescent()
    # @ctx.textSize Math.min(h * 0.5, s)
    # @ctx.translate 0, - textDescent() * 0.5

    @ctx.textSize h * 0.5
    offset =  (h / @ctx.textDescent()) * 0.5
    # console.log offset, h
    @ctx.text t, x + w * 0.5, y + h * 0.5 - offset
    @ctx.pop()
    @draw_border_local x,y,w,h, sw + 1, sc
    # @border sc, sw

  setup : (s) ->
    if @ctx?
      @ctx.textFont font

    @init s

  init : (s) ->

  render_quad : () ->
    image @ctx, @x, @y, @w, @h
  render : (s) ->
    if @visible
      if @dirty 
        @dirty = false
        @draw s
        if @debug 
          console.log("draw", @name)
        @render_quad()
      if s.dirty
        @render_quad()
    else
      if @ctx?
        @ctx.clear()


  draw : (s) ->
    @filled_text @name, @text_color, @back_color, @border_color, (if @down then 8 else 4)

  collide : (p) ->
    return @visible and p.x >= @x and p.x < @x + @w and p.y >= @y and p.y < @y + @h


create_gui = () ->

  menu_height = Math.ceil(innerHeight / 5)
  menu_width = menu_height
  menu_pos = innerWidth - menu_width

  _h = menu_height
  _w = menu_height
  _x = menu_pos


  bar : new Div "bar",
    x : innerWidth - _w
    y : -10
    w : _w
    h : 3000
    visible : false
    transparent : true
    dirty : false
    draw : (s) ->
    pan : (s) ->
      s.scroll_view(- s.pointer_delta.y)
      if s.scroll >= 0
        gui.settings_close_button.close_menu s, false
      else
        gui.settings_close_button.open_menu s, false
      # console.log menu[0].y
    panend : (s, e) ->
      s.snap_scroll e.deltaY


#============================================================================ MENU BUTTONS
  
  

  delete_all : new Div "[X]",
    x : _x
    y: 12
    w: _w
    h: _h
    tap: (s) ->
      if s.frames.length > 0
        start_confirm "delete ALL #{s.frames.length} frames?????", () -> s.remove_all()
      else
        start_confirm "nothing to remove", () ->
    draw : (s) ->
      @filled_text @name, colors.highlight, colors.bg, colors.fg, (if @down then 8 else 4)

  op_button : new Div "OP",
    x : _x
    y: 7
    w: _w
    h: _h
    tap : (s) ->
      gui.op_strip.op_mode = 1 - gui.op_strip.op_mode
      gui.op_strip.dirty = true

    press : (s) ->
    # draw : (s) ->
      # @fill_text (if @length_mode then "L" else "E"), colors.bg, colors.fg



  live : new Div "**",
    x : _x
    y: 4
    w: _w
    h: _h
    needs_update : (s) -> s.dirty
    tap: (s) ->
      s.toggle_include_live()
    draw: (s) ->
      if s.next_include_state and not s.include_live
        @filled_text "*-", colors.fg, colors.bg, colors.fg
      else if s.include_live
        @filled_text "**", colors.highlight, colors.bg, colors.fg
      else
        @filled_text "--", colors.fg, colors.bg, colors.fg
  
  frame_index : new Div "-",
    x : _x
    y : 3
    w : _w
    h : _h
    step_name : (s) ->
    needs_update : (s) -> s.dirty
    draw : (s) ->
      if s.play
        if s.include_live and s.play_pos is s.capture_pos
          @name = "**"
          @text_color = colors.highlight
        else
          @name = s.start + s.play_pos + 1
          @text_color = colors.fg
        @filled_text @name, @text_color, colors.bg
      else
        if s.frames.length > 0
          @filled_text s.selected_frame + 1,colors.fg, colors.bg
        else
          @filled_text "-",colors.fg, colors.bg

    # tap: click_length

  play : new Div ">",
    x : _x
    y : 2
    w : _w
    h : _h
    needs_update : (s) -> s.dirty
    tap: (s) ->
      s.toggle_play()
    draw : (s) ->
      if s.play
        @filled_text @name, colors.bg, colors.highlight, colors.bg
        # @filled_text @name, colors.highlight, colors.fg, colors.highlight
      else
        @filled_text @name, colors.bg, colors.fg, colors.bg

  play_mode : new Div ">>",
    x : _x
    y : 1
    w : _w
    h : _h
    play_modes : [">>", "><", "<<", "||", ">|"]
    press : (s) ->
      if s.play_mode > 2
        s.play_mode = 0
      else
        s.play = true
        s.play_mode = 3
      s.dirty = true
      @name = @play_modes[s.play_mode]
    needs_update : (s) -> s.dirty
    tap : (s) ->
      if s.play_mode > 2
        s.play_mode = 3 + (1 - (s.play_mode - 3))
      else
        s.play_mode = (s.play_mode + 1) % 3
        # s.play_mode = (s.play_mode + 1) % @play_modes.length

      s.dirty = true
      @name = @play_modes[s.play_mode]
    draw : (s) ->
      # n = if s.frames.length is 0 then "" else @name
      @filled_text @name, colors.fg, colors.bg, colors.fg


  fullscreen_button : new Div "[]",
    visible : false
    x : _x
    y : 2
    w : _w
    h : _h
    needs_update : (s) -> s.dirty
    draw : (s) ->
      @filled_text @name


    tap: (s) -> 
      screen_state = fullscreen()
      fullscreen(not screen_state)
      if isMobile.any and not screen_state
        screen.orientation.lock "landscape"

      # s.toggle_include_live true
      # s.selected_frame = s.capture_pos

  # fullscreen_button : new Div "[]",
  #   x : _x
  #   y : 0
  #   w : _w
  #   h : _h
  #   needs_update : (s) -> s.dirty
  #   draw : (s) ->
  #     @filled_text @name


  #   tap: (s) -> 
  #     screen_state = fullscreen()
  #     fullscreen(not screen_state)
  #     if isMobile.any and not screen_state
  #       screen.orientation.lock "landscape"

  #     # s.toggle_include_live true
  #     # s.selected_frame = s.capture_pos

  settings_close_button : new Div ")(",
    x : _x
    y : -2
    w : _w
    h : _h
    needs_update : (s) -> s.dirty
    tap : (s) ->
      @close_menu s
    close_menu : (s, set_scroll = true) ->
      if set_scroll 
        s.scroll = 0
      s.settings_view = false
      gui.fullscreen_button.show false
      gui.onion.show false
      gui.onion_button.show false
      gui.camera_settings_button.show false
      gui.absonion_button.show false
      @dirty = true
      s.dirty = true
      gui.settings_menu_button.visible = true
      gui.play_mode.dirty = true

  settings_menu_button : new Div "()",
    x : _x
    y : 0
    w : _w
    h : _h
    needs_update : (s) -> s.dirty

    draw : (s) ->
      if s.play
        @ctx.fill colors.bg
        @ctx.rect 0, 0, @w, @h
        @border colors.fg, 3
        # @border colors.highlight, 3
        @ctx.noStroke()
        @ctx.textAlign CENTER, CENTER
        @ctx.fill colors.fg
        @ctx.textSize @h * 0.35
        @ctx.text Math.round(s.fps), 0.5 * @w, 0.30 * @h - textDescent() * 0.5
        @ctx.text "fps", 0.5 * @w, 0.65 * @h - textDescent() * 0.5
      else
        @name = if s.scroll >= 0 then "()" else ")("
        @filled_text @name
    init : (s) ->
      if s.settings_view
        @open_menu s

    select_menu : (s, b) ->
      s.selected_setting = b
      gui.absonion.show false
      gui.onion.show false
      gui.camera_settings.show false

      gui.absonion_button.highlight false
      gui.onion_button.highlight false
      gui.camera_settings_button.highlight false

      gui[b + "_button"].highlight()
      gui[b].show true
    open_menu : (s, set_scroll = true) ->
      if set_scroll 
        s.scroll = - 2 * menu_height
      s.settings_view = true
      @visible = false


      gui.fullscreen_button.show()
      gui.onion_button.show()
      gui.camera_settings_button.show()
      gui.absonion_button.show()
      @select_menu s, s.selected_setting
      @dirty = true



    tap: (s) ->
      @open_menu s
      s.dirty = true
      
  frame_strip : new Div "frame_strip",
    x : 0
    y : 5
    # y : - _h
    w : menu_pos
    h : _h
    # setup : (s) ->
      # @canvas = createGraphics()
    offset : 0

    press_pan : false
    tap : (s) ->
      lp = @local_pointer s
      s.set "selected_frame", Math.floor(s.start + (s.get_loop_length()) * clamp(lp.x, 0, 0.9999))
    press : (s) ->
      lp = @local_pointer s
      s.set "capture_pos", Math.floor(s.start + (s.get_loop_length()) * clamp(lp.x, 0, 0.9999))
      s.set "selected_frame", s.capture_pos
      # @press_pan = true

    pan : (s) ->
      @offset += - s.pointer_delta.x * (s.get_loop_length() / @w)

      if Math.abs(@offset) > 1
        s.change_start Math.round(@offset), false
        @offset = 0
        @x = 0
    panend : (s) ->
      if @press_pan
        lp = @local_pointer s
        c = Math.floor(s.start + (s.get_loop_length()) * clamp(lp.x, 0, 0.9999))
        console.log c, s.capture_pos
        if c isnt s.capture_pos
          press_pan = false
          s.modify_frames (if s.include_live then "move" else "swap"), s.capture_pos, c
          s.capture_pos = c
      else
        @offset = 0
        @x = 0
        @dirty = true
        s.dirty = true
        # console.log lp
    needs_update : (s) -> s.dirty or @dirty

    draw : (s) ->
      if s.include_live and not s.camera_handler.loaded 
        return 
      @local_clear_rect()
      if s.frames.length > 0
        l = (s.end + 1) - s.start
        w = @w /  l
        # w = viewport_width /  l

        i = 0
        h = @h
        for fi in [s.start..clamp(s.end, s.start, s.get_length() - 1)]
          f = s.get_frame fi
          if not f?
            console.log "no image"
            return
          @ctx.noStroke()
          @ctx.fill colors.white
          if fi is s.capture_pos and s.include_live
            @ctx.rect i * w, 0, w, @h
            @ctx.blendMode DIFFERENCE
            @ctx.image(f, i * w, 0, w, @h)
            @ctx.blendMode BLEND
          else
            @ctx.image(f, i * w, 0, w, @h)

          w2 = w * 0.5
          h2 = h * 0.2
          if fi is s.selected_frame
            # @ctx.fill colors.white
            # @ctx.rect i * w, 0 + @h - h2, w, h2
            @draw_border_local i * w, 0, w, h, 6, colors.white
          if s.capture_pos is fi
            b = if s.capture_pos is s.selected_frame then 6 else 0
            if s.include_live
              @draw_border_local i * w + b, b, w - b - b, h - b - b, 6
            else
              @draw_border_local i * w + b, b, w - b - b, h - b - b, 6, colors.fg
          i++

        if s.play
          @ctx.noStroke()
          @ctx.blendMode DIFFERENCE
          @ctx.fill colors.white
          @ctx.ellipse s.play_pos * w + w * 0.5, 0 + h * 0.5, Math.min(h * 0.3, w * 0.3)
          @ctx.blendMode BLEND
      else
        # @clear_rect()
        @filled_text "tap to capture frame", colors.fg, colors.bg

        # image(s.camera_image, @x, @y, s.aspect * @h, @h)
        # @draw_border @x, @y, s.aspect * @h, @h, 4

  remove : new Div "X",
    x : _x
    y: 5
    w: _w
    h: _h
    tap: (s) ->
      if s.frames.length is 0
        start_confirm "nothing to remove", () ->
      else
        if s.include_live and s.selected_frame is s.capture_pos
          start_confirm "can't remove live image", () ->
        else
          start_confirm "delete frame #{s.selected_frame + 1}?", () -> s.remove_selected()
    press : (s) ->
      




  length_mode : new Div "length_mode",
    x : _x
    y: 6
    w: _w
    h: _h
    length_mode : false
    tap : (s) ->
      @length_mode = not @length_mode
    press : (s) ->
      s.reset_loop()
    draw : (s) ->
      @filled_text (if @length_mode then "L" else "E"), colors.fg, colors.bg

  length_strip : new Div "length_strip",
    x : 0
    y : 6
    # y : - _h * 2
    w : menu_pos
    h : _h
    offset : 0
    set_start : false
    needs_update : (s) -> s.dirty
    press : (s) ->
      s.reset_loop()
    tap : (s) ->
      if s.pointer.x < @h
        @set_start = true
        @dirty = true
      else if s.pointer.x > @w - @h
        @set_start = false
        @dirty = true

      x = @h
      w = @w - 2 * @h
      lx = x + w * (s.start / s.get_length())
      lw = w * (s.get_loop_length() / s.get_length())

      threshold = @w * 0.1
      d_start = abs(s.pointer.x - lx)
      d_end = abs(s.pointer.x - (lx + lw))

      if d_start < d_end
        if d_start < threshold
          @set_start = true
          @dirty = true
      else
        if d_end < threshold
          @set_start = false
          @dirty = true


    panstart : (s, e) ->
      s.pointer = e.center
      @tap s
    pan : (s) ->
      @offset += s.pointer_delta.x * (s.get_length() / (@w - @h - @h - @h))
      # @offset += s.pointer_delta.x * (1 / (@w / (s.get_loop_length())))
      if Math.abs(@offset) > 1
        if @set_start
          s.change_start Math.round(@offset), not gui.length_mode.length_mode
        else
          if gui.length_mode.length_mode
            s.change_length Math.round(@offset)
          else
            s.change_end Math.round(@offset)
        @offset = 0
      # s.change_length s.pointer_delta.x *
      # console.log s
    draw : (s) ->
      @local_clear_rect()

      if s.frames.length > 0
        l = s.get_length()
        x = 0
        # w = @w - 2 * @h
        w = @w
        cw = w / l
        cr = Math.min cw * 0.25, @h * 0.5

        @ctx.noStroke()
        @ctx.fill 60
        @ctx.rect x, 0, w, @h
        @ctx.fill colors.fg
        lx = x + w * (s.start / l)
        lw = w * (s.get_loop_length() / l)
        selected_frame_x = w * (s.selected_frame / l)
        capture_pos_x = w * (s.capture_pos / l)
        @ctx.rect lx, 0, lw, @h

        # @ctx.push()
        # @ctx.translate 0, -@ctx.textDescent() * 2

        # @ctx.fill colors.white
        # @ctx.textAlign CENTER, CENTER
        # @ctx.textSize @h * 0.5
        # @ctx.text s.start + 1, @x + @h * 0.5, 0 + @h * 0.5
        # @ctx.text s.end + 1, @x + @w - @h * 0.5, 0 + @h * 0.5

        # # @ctx.fill colors.bg
        # # @ctx.text s.get_loop_length(), lx + lw * 0.5, 0 + @h * 0.5
        # @ctx.pop()

        @ctx.push()
        y = @h * 0.5
        @ctx.translate x + cw * 0.5, y
        @ctx.fill 0,0,0,160
        for i in [0..l - 1]
          x = i * cw
          @ctx.circle x, 0, cr



        @ctx.pop()


        if s.include_live
          @ctx.noStroke()
          @ctx.fill colors.highlight
          @ctx.circle capture_pos_x + cw * 0.5, y, cr

        @ctx.noFill()
        @ctx.stroke colors.white
        @ctx.strokeWeight 6
        @ctx.circle selected_frame_x + cw * 0.5, y, (cr - 4)

        # if @set_start
        end_pad = @h * 0.15 * (s.end+1 + "").length
        @text s.start + 1, clamp(lx + cw * 0.25, end_pad, @w - end_pad), @h * 0.25, colors.white, @h * 0.3
        @text s.end + 1, clamp(lx + lw - cw + cw * 0.75, end_pad, @w - end_pad), @h * 0.8, colors.white, @h * 0.3
        # @ctx.circle lx + cw * 0.5, y, cr
        # @ctx.text s.end + 1, @x + @w - @h * 0.5, 0 + @h * 0.5
        # else
          # @ctx.circle lx + lw - cw + cw * 0.5, y, cr
        # @ctx.pop()
      else
        @filled_text "no frames"

  op_strip : new Div "op_strip",
    x : 0
    y : 7
    # y : - _h * 3
    w : menu_pos
    h : _h
    op_mode : 0
    needs_update : (s) -> s.dirty
    tap : (s) ->
      if @op_mode is 0
        s.modify_frames (if s.include_live then "move" else "swap"), s.selected_frame, s.capture_pos
      else
        s.modify_frames (if s.include_live then "crop" else "delete_loop"), s.selected_frame, s.capture_pos



    arrow : (x, y, length, arrow_width, arrow_height, sw = 4, c = colors.fg) ->
      @ctx.stroke colors.fg
      @ctx.strokeWeight 4
      @ctx.line x, y, x + length, y
      arrow_start = if arrow_width > 0 then x + length - length * arrow_width else x + Math.abs(arrow_width) * length
      arrow_end = if arrow_width > 0 then x + length else x
      @ctx.line arrow_start, y - arrow_height, arrow_end, y
      @ctx.line arrow_start, y + arrow_height, arrow_end, y
    draw : (s) ->
      @local_clear_rect()
      if @op_mode is 0
        # @filled_text (if s.include_live then "move" else "swap"), colors.fg, colors.bg
        @text (if s.include_live then "move" else "swap"), @w * 0.3333, @h * 0.5, colors.white,  @h * 0.5
        h = @h * 0.6
        h2 = (@h - h) * 0.5
        x = @w * 0.5 - h * 1.5
        ax = x + h + h2 + h2 * 0.5
        # x = h + h2 + h2 * 0.5

        if s.include_live
          @arrow ax, @h * 0.5, h2, 0.6, @h * 0.15
        else
          @arrow ax, @h * 0.35, h2, 0.6, @h * 0.1
          @arrow ax, @h * 0.65, h2, -0.6, @h * 0.1

        @draw_border_local x + @h + h2, h2, h, h, 6, (if s.include_live then colors.highlight else colors.fg)
        @draw_border_local x + h2, h2, h, h, 6, colors.white

      else
        @filled_text (if s.include_live then "crop" else "delete loop"), colors.fg, colors.bg, colors.bg
      # @fill_text (if s.include_live then "move" else "swap"), colors.fg, colors.bg


  camera_settings_button : new Div "%",
    visible : false

    x : _x
    y: 0
    w: _w
    h: _h
    tap: (s) -> 
      if s.selected_setting isnt "camera_settings"
        gui.settings_menu_button.select_menu s, "camera_settings"
      else
        s.step_setting()
    press : (s) ->
      if s.selected_setting isnt "camera_settings"
        gui.settings_menu_button.select_menu s, "camera_settings"
      else
        s.toggle_torch()

  camera_settings : new Div "camera_settings",
    x : 0
    y : -1

    # y : innerHeight
    w : innerWidth - _w
    h : _h
    needs_update : (s) -> s.dirty or @dirty

    press : (s) ->
      s.set_auto s.setting
    pan : (s) ->
      s.set_setting s.setting, (s.pointer_delta.x / @w)
      @dirty = true

    draw : (s) ->
      # @clear_rect()
      if s.setting is -1
        @filled_text "no settings available ", colors.bg, colors.fg, colors.fg

        false
      else
        set = s.camera_handler.settings[s.setting]
        v = 
          if set.auto
            "auto"
          else
            if set.step < 1 then set.value.toFixed(2) else Math.floor(set.stepped())
        @filled_text set.readable_name + " : " + v, colors.bg, colors.fg, colors.fg

        @ctx.noStroke()
        @ctx.fill 255,255,255,60
        @ctx.rect 0,0, set.normalized() * @w, @h

  # download : new Div "_",
  #   x : _x
  #   y: -2
  #   w: _w
  #   h: _h
  #   tap : (s) ->
  #     download_frames()
  #     # console.log "DOWNLOAD"
  #   # tap: download_frames

  project_strip : new Div "project",
    x : 0
    y : -2
    # y : - _h * 3
    w : innerWidth - _w
    h : _h
    tap : (s, e) ->
      # TODO FIX 
      setTimeout (() -> ui.details_panel.show()), 100
      
    needs_update : (s) -> s.dirty
    draw : (s) ->
      @filled_text "#{s.title} [#{s.frames.length}] #{s.camera_handler.width} x #{s.camera_handler.height}", colors.fg, colors.bg, colors.bg

  frame_view : new Div "frame_view",
    x : 0
    y : 0
    w : menu_pos
    h : _h * 5
    offset : 0
    press : (s) ->
      if not s.play
        if not s.include_live
          s.toggle_include_live true
        else
          s.set "capture_pos", s.selected_frame
          # s.set "selected_frame", s.capture_pos

    pan : (s) ->
      lp = @local_pointer s
      if s.settings_view
        switch s.selected_setting 
          when "onion"
            gui.onion.set_opacity s
          when "camera_settings"
            gui.camera_settings.pan s
          else
            console.log "TODO ABSONION"
      else if s.play
        s.change_fps (s.pointer_delta.x / @w) * 60.0
      else
        @offset += s.pointer_delta.x * (7 / @w)
        # @offset += s.pointer_delta.x * (1 / (@w / (s.get_loop_length())))
        if Math.abs(@offset) > 1
          o = parseInt(@offset.toFixed(0))
          console.log @offset, o
          s.set "selected_frame", wrap(s.selected_frame + o, s.get_length())
          @offset = 0
        # s.set "selected_frame", Math.floor(s.start + (s.get_loop_length()) * clamp(lp.x, 0, 0.9999))

    panstart : (s) ->
      if not s.play
        if s.scroll is 0
          gui.frame_strip.y = _h * 5 - 6
    panend : (s) ->
      if not s.play
        if s.scroll is 0
          gui.frame_strip.y = _h * 5
    tap : (s) ->
      lp = @local_pointer s
      if not s.play
        if s.selected_frame is s.capture_pos
          state.capture_frame()
      else
        switch s.play_mode
          when PLAY_LOOP, PLAY_PINGPONG, PLAY_REVERSE, PLAY_ONCE
            s.toggle_play()
          # when 1
          #   s.reset_play()
          when PLAY_STEPPED
            s.step_play_pos(if lp.x > 0.5 then 1 else -1)
    get_alpha : (max, a, i) ->
      step = max / a
      max - (i * step)
    draw_onion : (x, y, w, h, i, s) ->
      # @local_clear_rect()
      # console.log onion
      # a = 255 / onion.length
      # @ctx.tint 255, a
      # noTint()

      selected_frame = s.get_frame i
      pres = s.get_pre_onion i
      posts = s.get_post_onion i

      # console.log pres, posts

      if pres.length > 0 or posts.length > 0
        c = ctx.canvas.getContext("2d")

        image selected_frame, @x + x, @y + y, w, h

        if pres.length > 0
          for pre in [pres.length - 1..0]
            a = @get_alpha s.onion.max_opacity, s.onion.pre, pre
            c.globalAlpha = a
            # console.log SCREEN, OVERLAY, BURN, SUBTRACT, DIFFERENCE, MULTIPLY, HARD_LIGHT, DODGE
            blendMode s.onion.get_blend()
            image pres[pres.length - pre - 1], @x + x, @y + y, w, h
            blendMode BLEND

        if posts.length > 0
          for post in [posts.length - 1..0]
            a = @get_alpha s.onion.max_opacity, s.onion.post, post
            c.globalAlpha = a
            image posts[post], @x + x, @y + y, w, h

        c.globalAlpha = 1
      else
        image selected_frame, @x + x, @y + y, w, h
    debug : false
    last_scroll : 0

    needs_update : (s) ->
      # @dirty = true
      if s.scroll isnt @last_scroll
        @last_scroll = s.scroll
        true
      else if s.include_live
        s.dirty or s.camera_handler.dirty
        # (s.camera_visible() and s.camera_dirty) or s.dirty
      else
        s.dirty
    # needs_update : (s) -> s.camera_dirty or s.dirty
    render : (s) ->
      if @visible
        if @dirty
          @dirty = false
          @draw s
        #   console.log "rendering"
        # else
        #   console.log "not rendering"
    clear_rect : () ->
      fill colors.bg
      noStroke()
      rect @x, @y, @w, @h
    draw : (s) ->
      @clear_rect()
      # @local_clear_rect()
      if s.camera_handler.loaded or not s.include_live
        h = innerHeight - abs(s.scroll)
        w = h * s.aspect
        if w > @w
          w = clamp w, 0, @w
          h = clamp (w * (1 / s.aspect)), 0, @h

        if w <= 0 or h <= 0
          return

        x = @x + @w * 0.5 - w * 0.5

        y = 
          if s.scroll > 0 
            @h - h
          else if s.scroll < 0
            0
          else
            @h * 0.5 - h * 0.5
        # y = if s.scroll > 0 then (@h - h) else @h * 0.5 - h * 0.5

        if s.play
          if s.onion.enabled
            @draw_onion x, y, w, h, s.start + s.play_pos, s
          else
            f = s.get_frame(s.start + s.play_pos)
            if f?
              image f, x, y, w, h
        else
          if s.onion.enabled
            @draw_onion x, y, w, h, s.selected_frame, s
          else
            @clear_rect()
            f = s.get_frame s.selected_frame
            if f?
              image f, @x + x, @y + y, w, h
            # @ctx.image f, x, y, w, h

          if s.include_live and s.selected_frame is s.capture_pos
            @draw_border @x + x, @y + y, w, h, 5, colors.highlight
      else
        if not s.camera_handler.loaded and s.include_live and s.capture_pos is s.selected_frame
          @filled_text ""
          image @ctx, @x, @y, @w, @h


      # @clear_rect()

  onion_button : new Div "O",
    visible : false
    x : _x
    y: -1
    w: _w
    h: _h
    
    press : (s) ->
      if s.selected_setting isnt "onion"
        gui.settings_menu_button.select_menu s, "onion"
      else
        # gui.settings_menu_button.select_menu s, "onion"
        s.dirty = true
        gui.onion.blend_setting = not gui.onion.blend_setting        
        s.onion.enabled = true
        gui.onion.dirty = true
    tap: (s) ->
      if s.selected_setting isnt "onion"
        gui.settings_menu_button.select_menu s, "onion"
      else
        if not gui.onion.blend_setting
          gui.onion.opacity_setting = not gui.onion.opacity_setting
          gui.onion.dirty = true
        else
          gui.onion.blend_setting = false
          gui.onion.dirty = true

    draw : (s) ->
      @filled_text (if gui.onion.blend_setting then "O~" else if gui.onion.opacity_setting then "O#" else "O"), @text_color, @back_color, @border_color, (if @down then 8 else 4)

  onion : new Div "onion",
    visible : false
    x : 0
    y : -1
    w : menu_pos
    h : _h
    tap : (s) ->
      if @blend_setting
        console.log s
        p = @local_pointer s
        d = if p.x > 0.5 then 1 else -1
        s.onion.blend_mode = wrap s.onion.blend_mode + d, s.onion.blend_modes.length
        s.dirty = true
        @dirty = true
      else 
        if not @opacity_setting
          p = s.pointer.x 
          w = @center_w()
          if p < @h
            s.set_onion "pre", 1
          else if p < @h * 2
            s.set_onion "pre", -1
          else if p < @h * 2 + w
            s.toggle_onion()
          else if p < @h * 3 + w
            s.set_onion "post", -1
          else
            s.set_onion "post", 1
    onioned_text : (s) ->
      if s.onion.enabled
        l = repeated "(", s.onion.pre
        e = repeated ")", s.onion.post
        l + "onion" + e 
      else 
        "nonion"
    opacity_setting : false
    needs_update : (s) -> s.dirty
    center_w : () -> @w - 4 * @h
    blend_setting : false

    draw_blending : (s) ->
      @filled_text "<    " + s.onion.get_blend() + "    >"

    press : (s) ->
      if @blend_setting
        s.onion.blend_mode = 0
      else
        if @opacity_setting
          s.onion.max_opacity = s.onion.default_opacity
        else
          s.onion.pre = 1
          s.onion.post = 1
      s.dirty = true
    draw : (s) ->
      if @blend_setting
        @draw_blending s
      else
        if @opacity_setting
          @draw_slider s
        else
          @draw_slices s
    set_opacity : (s) ->
      s.set_onion_opacity(s.pointer_delta.x / @w)
    pan : (s) ->
      if @opacity_setting and not @blend_setting
        # lp = @local_pointer s
        @set_opacity s

      if @blend_setting
        console.log s

    draw_slider : (s) ->
      v = s.onion.max_opacity
      @filled_text "onion opacity : #{Math.floor(s.onion.max_opacity * 100)}%", colors.bg, colors.fg, colors.fg

      @ctx.noStroke()
      @ctx.fill 255,255,255,60
      @ctx.rect 0,0, v * @w, @h

    draw_slices : (s) ->
      percent = get_free_memory()

      used = 100 - percent

      frame_cost = (used / s.frames.length).toFixed(2)

      max_frames = Math.floor(90 / frame_cost)

      w = @center_w()
      t = @onioned_text s
      @filled_text "+",
        colors.fg, colors.bg, colors.fg, 3
        0, 0, @h, @h
      @filled_text "-",
        colors.fg, colors.bg, colors.fg, 3,
        1 * @h, 0, @h, @h
      @filled_text t,
        colors.fg, colors.bg, colors.fg, 3
        2 * @h, 0, w, @h
      @filled_text "-",
        colors.fg, colors.bg, colors.fg, 3
        2 * @h + w, 0, @h, @h
      @filled_text "+",
        colors.fg, colors.bg, colors.fg, 3,
        3 * @h + w, 0, @h, @h

  absonion : new Div "placeholder",
    visible : false
    x : 0
    y: -1
    w: menu_pos
    h: _h


  absonion_button : new Div "G",
    visible : false
    x : _x
    y: 1
    w: _w
    h: _h
    tap : (s) ->
      if s.selected_setting isnt "absonion"
        gui.settings_menu_button.select_menu s, "absonion"
      else
        0

  bottom : new Div "_",
    visible : false
    x : _x
    y: 8
    w: _w
    h: _h
    # tap: click_length


get_top = () -> 0
# get_top = () -> - menu_height * 4 
get_bottom = () -> 
  y_offset = gui.bottom.y - gui.frame_view.h
  b = Math.max(y_offset - Math.max(Math.floor(innerHeight / menu_height) - 6, 0) * menu_height, 0)
  b

init_gui = () ->
  window.menu = []
  window.gui = create_gui()
  for k, v of window.gui
    v.setup(state)
    v.y *= menu_height
  window.menu = [
    gui.settings_menu_button
    gui.play_mode
    gui.play
    gui.frame_index
    gui.live
    gui.remove
    gui.length_mode
    gui.op_button
    gui.delete_all
    gui.camera_settings_button
    gui.download
  ]



# draw_grid = () ->
#   l = 3
#   w = state.viewport_width / l
#   h = state.viewport_height / l
#   stroke 255
#   noFill()
#   for x in [0..l - 1]
#     for y in [0..l - 1]
#       rect w * x, y * h, w, h