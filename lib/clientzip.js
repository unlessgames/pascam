"stream" in Blob.prototype ||
    Object.defineProperty(Blob.prototype, "stream", {
        value() {
            return new Response(this).body;
        },
    }),
    "setBigUint64" in DataView.prototype ||
        Object.defineProperty(DataView.prototype, "setBigUint64", {
            value(e, n, t) {
                const i = Number(0xffffffffn & n),
                    o = Number(n >> 32n);
                this.setUint32(e + (t ? 0 : 4), i, t), this.setUint32(e + (t ? 4 : 0), o, t);
            },
        });
var e = (e) => new DataView(new ArrayBuffer(e)),
    n = (e) => new Uint8Array(e.buffer || e),
    t = (e) => Math.min(4294967295, Number(e)),
    i = (e) => Math.min(65535, Number(e));
function o(e, t, i) {
    if ((void 0 === t || t instanceof Uint8Array || (t = r(t)), void 0 === i || i instanceof Date || (i = new Date(i)), e instanceof File)) return { t: t || r(e.name), i: i || new Date(e.lastModified), o: e.stream() };
    if (e instanceof Response) {
        const n = e.headers.get("content-disposition"),
            o = n && n.match(/;\s*filename\*?=["']?(.*?)["']?$/i),
            f = (o && o[1]) || new URL(e.url).pathname.split("/").pop(),
            a = f && decodeURIComponent(f);
        return { t: t || r(a), i: i || new Date(e.headers.get("Last-Modified") || Date.now()), o: e.body };
    }
    if (!t || 0 === t.length) throw new Error("The file must have a name.");
    if (void 0 === i) i = new Date();
    else if (isNaN(i)) throw new Error("Invalid modification date.");
    if ("string" == typeof e) return { t, i, o: r(e) };
    if (e instanceof Blob) return { t, i, o: e.stream() };
    if (e instanceof Uint8Array || e instanceof ReadableStream) return { t, i, o: e };
    if (e instanceof ArrayBuffer || ArrayBuffer.isView(e)) return { t, i, o: n(e) };
    if (Symbol.asyncIterator in e) return { t, i, o: f(e) };
    throw new TypeError("Unsupported input format.");
}
function f(e) {
    const n = "next" in e ? e : e[Symbol.asyncIterator]();
    return new ReadableStream({
        async pull(e) {
            let t = 0;
            for (; e.desiredSize > t; ) {
                const i = await n.next();
                if (!i.value) {
                    e.close();
                    break;
                }
                {
                    const n = a(i.value);
                    e.enqueue(n), (t += n.byteLength);
                }
            }
        },
    });
}
function a(e) {
    return "string" == typeof e ? r(e) : e instanceof Uint8Array ? e : n(e);
}
function r(e) {
    return new TextEncoder().encode(String(e));
}
var s = new WebAssembly.Instance(
        new WebAssembly.Module(
            Uint8Array.from(
                atob(
                    "AGFzbQEAAAABCgJgAABgAn9/AXwDAwIAAQUDAQACBwkCAW0CAAFjAAEIAQAKlQECSQEDfwNAIAEhAEEAIQIDQCAAQQF2IABBAXFBoIbi7X5scyEAIAJBAWoiAkEIRw0ACyABQQJ0IAA2AgAgAUEBaiIBQYACRw0ACwtJAQF/IAFBf3MhAUGAgAQhAkGAgAQgAGohAANAIAFB/wFxIAItAABzQQJ0KAIAIAFBCHZzIQEgAkEBaiICIABJDQALIAFBf3O4Cw"
                ),
                (e) => e.charCodeAt(0)
            )
        )
    ),
    { c, m } = s.exports,
    A = n(m).subarray(65536);
function d(e, n = 0) {
    for (const t of (function* (e) {
        for (; e.length > 65536; ) yield e.subarray(0, 65536), (e = e.subarray(65536));
        e.length && (yield e);
    })(e))
        A.set(t), (n = c(t.length, n));
    return n;
}
function u(e, n, t = 0) {
    const i = (e.getSeconds() >> 1) | (e.getMinutes() << 5) | (e.getHours() << 11),
        o = e.getDate() | ((e.getMonth() + 1) << 5) | ((e.getFullYear() - 1980) << 9);
    n.setUint16(t, i, 1), n.setUint16(t + 2, o, 1);
}
function y(t) {
    const i = e(30);
    return i.setUint32(0, 1347093252), i.setUint32(4, 754976768), u(t.i, i, 10), i.setUint16(26, t.t.length, 1), n(i);
}
async function* l(e) {
    let { o: n } = e;
    if (("then" in n && (n = await n), n instanceof Uint8Array)) yield n, (e.A = d(n, 0)), (e.u = BigInt(n.length));
    else {
        e.u = 0n;
        const t = n.getReader();
        for (;;) {
            const { value: n, done: i } = await t.read();
            if (i) break;
            (e.A = d(n, e.A)), (e.u += BigInt(n.length)), yield n;
        }
    }
}
function w(i, o) {
    const f = e(16 + (o ? 8 : 0));
    return f.setUint32(0, 1347094280), f.setUint32(4, i.A, 1), o ? (f.setBigUint64(8, i.u, 1), f.setBigUint64(16, i.u, 1)) : (f.setUint32(8, t(i.u), 1), f.setUint32(12, t(i.u), 1)), n(f);
}
function b(i, o, f = 0) {
    const a = e(46);
    return (
        a.setUint32(0, 1347092738),
        a.setUint32(4, 755182848),
        a.setUint16(8, 2048),
        u(i.i, a, 12),
        a.setUint32(16, i.A, 1),
        a.setUint32(20, t(i.u), 1),
        a.setUint32(24, t(i.u), 1),
        a.setUint16(28, i.t.length, 1),
        a.setUint16(30, f, 1),
        a.setUint16(40, 33204, 1),
        a.setUint32(42, t(o), 1),
        n(a)
    );
}
function B(t, i, o) {
    const f = e(o);
    return f.setUint16(0, 1, 1), f.setUint16(2, o - 4, 1), 16 & o && (f.setBigUint64(4, t.u, 1), f.setBigUint64(12, t.u, 1)), f.setBigUint64(o - 8, i, 1), n(f);
}
export const downloadZip = (a, progress) =>
    new Response(
        f(
            (async function* (o) {
                const f = [];
                let a = 0n,
                    r = 0n,
                    s = 0;
                let frame_index = 0
                for await (const e of o) {
                    progress(frame_index++)
                    yield y(e), yield e.t, yield* l(e);
                    const n = e.u >= 0xffffffffn,
                        t = (12 * +(a >= 0xffffffffn)) | (28 * +n);
                    yield w(e, n), f.push(b(e, a, t)), f.push(e.t), t && f.push(B(e, a, t)), n && (a += 8n), r++, (a += BigInt(46 + e.t.length) + e.u), s || (s = n);
                }
                let A = 0n;
                for (const e of f) yield e, (A += BigInt(e.length));
                if (s || a >= 0xffffffffn) {
                    const t = e(76);
                    t.setUint32(0, 1347094022),
                        t.setBigUint64(4, BigInt(44), 1),
                        t.setUint32(12, 755182848),
                        t.setBigUint64(24, r, 1),
                        t.setBigUint64(32, r, 1),
                        t.setBigUint64(40, A, 1),
                        t.setBigUint64(48, a, 1),
                        t.setUint32(56, 1347094023),
                        t.setBigUint64(64, a + A, 1),
                        t.setUint32(72, 1, 1),
                        yield n(t);
                }
                const d = e(22);
                d.setUint32(0, 1347093766), d.setUint16(8, i(r), 1), d.setUint16(10, i(r), 1), d.setUint32(12, t(A), 1), d.setUint32(16, t(a), 1), yield n(d);
            })(
                (async function* (e) {
                    for await (const n of e) n instanceof File || n instanceof Response ? yield o(n) : yield o(n.input, n.name, n.lastModified);
                })(a)
            )
        ),
        { headers: { "Content-Type": "application/zip", "Content-Disposition": "attachment" } }
    );
