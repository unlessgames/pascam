# coffeeify = require "coffeeify"
# browserify = require "browserify"
qrcode = require "qrcode-terminal"
ip = require 'ip'
assert = require "assert"
fs = require "fs"
express = require "express"
body_parser = require "body-parser"

image_dir = './images';

ensure_directory = (d) ->
  if not fs.existsSync(d)
    fs.mkdirSync(d)

ensure_directory image_dir

process.env.PORT = 8080

repeated = (t, c) ->
  if c is 0 then ""
  else
    [0..c-1].fill(t).join ""

zeropad = (v, max) ->
  c = (max + "").length - 1
  s = v + ""
  repeated("0", c) + s

save_frame = (f) ->
  captured_frames += 1
  n = "#{f.name}_#{f.index}_#{get_time_string()}.png"
  console.log "new frame", n
  dir = "#{image_dir}/#{f.name}"
  ensure_directory dir


  fs.writeFileSync("#{dir}/#{n}", Buffer.from(f.data.replace(/^data:image\/png;base64,/, ""), 'base64'), () -> console.log("saved frame as #{n}"));

app = express()


print_links = () ->
  address = "http://#{ip.address()}:#{process.env.PORT}"
  console.log "\n[ CAM ] #{address}\n"
  qrcode.generate(address,{small: false});

app.use (req, res, next) ->
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()

app.use(body_parser.urlencoded({limit: '100mb', extended: true}));

app.post('/new_frame', (req, res, next) ->
  save_frame(req.body)
  res.sendStatus(200)
)

app.use express.static('.')

app.get('/lib/socket.io.min.js', (req, res) ->
    res.sendFile(__dirname + '/node_modules/socket.io/client-dist/socket.io.min.js');
)

listener = app.listen(process.env.PORT, print_links)

io = require("socket.io")(listener)

captured_frames = 0

get_time_string = () ->
  d = new Date().toLocaleString()
  date = d.split(",")[0].replaceAll "/", "-"
  t = d.split(", ")[1].split(":").slice(0,2).join("-")
  t + "_" + date

connect = (socket) ->
  console.log "client connected"
  socket.on "new_frame", save_frame
  socket.on "disconnecting", () -> console.log "client disconnected."
  socket.on "disconnect", () -> console.log "client disconnected."

io.on "connection", connect
# io.of("/user").on("connection", init_user)
