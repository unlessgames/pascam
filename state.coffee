PLAY_LOOP = 0
PLAY_PINGPONG = 1
PLAY_REVERSE = 2
PLAY_STEPPED = 3
PLAY_ONCE = 4

class State 
  constructor : () ->

  frames : []
  capture : null
  title : "frames"
  pan_target : null
  start : 0
  end : 0
  capture_pos : 0
  play_pos : 0
  play : false
  viewport_width : 100
  viewport_height : 100
  aspect : 1
  selected_frame : 0
  menu_height : innerHeight / 5
  loaded : false
  camera_handler : new Capturer()
  settings_view : false
  scroll : 0
  scroll_target : 0
  scroll_time : 0

  on_before_defocus : false
  pointer : x : 0, y : 0
  pointer_delta : x : 0, y : 0
  include_live : true
  onion : 
    get_blend : () ->
      @blend_modes[@blend_mode]
    blend_modes : ["source-over", "multiply", "hard-light", "difference", "color-dodge", "screen", "overlay", "color-burn"]
    blend_mode : 0
    enabled : false
    pre : 1
    post : 0
    max_opacity : 0.55
    default_opacity : 0.55
  capturing : false

  selected_setting : "onion"

  settings : []
  setting : -1
  time : 0
  last_frame_time : 0
  fps : 15
  frame_delta : 66.6666
  play_mode : 0

  next_include_state : true

  scroll_determined : false
  scroll_deadzone : 20
  cycle : 0

  get_onion : (i) ->
    s = clamp i - @onion.pre, 0, @get_length() - 1
    e = clamp i + @onion.post, 0, @get_length() - 1

    for i in [s..e]
      frame : @get_frame i
      index : i
  get_pre_onion : (i) ->
    s = clamp i - @onion.pre, 0, @get_length() - 1
    preframes = i - s
    if preframes > 0
      for j in [0..preframes - 1]
        @get_frame s + j
    else
      []
  get_post_onion : (i) ->
    e = clamp i + @onion.post, 0, @get_length() - 1
    postframes = e - i
    if postframes > 0
      for j in [1..postframes]
        @get_frame i + j
    else
      []
  
  camera_visible : () ->
    o = @get_onion @selected_frame
    for f in o
      if f.is_camera
        return true
    false
  toggle_onion : () ->
    @onion.enabled = not @onion.enabled
    if @onion.pre + @onion.post is 0
      @onion.pre = 1
    @dirty = true

  set_onion : (t, d) ->
    @onion[t] = clamp @onion[t] + d, 0, 3
    if not @onion.enabled
      @onion.enabled = true
    else if @onion.pre + @onion.post is 0
      @onion.enabled = false
    @dirty = true
  set_onion_opacity : (d) ->
    @onion.max_opacity = clamp @onion.max_opacity + d, 0.1, 0.9
    # console.log @onion.max_opacity
    @dirty = true
  set : (t, v) ->
    @[t] = v
    @dirty = true

  capture_ready : () ->
    @set_include_live()

    if @setting is -1
      @setting = clamp 0, -1, @camera_handler.settings.length - 1
    @dirty = true

  capture_frame : () ->
    # capture_frame()
    @camera_handler.trigger @add_frame.bind @

  add_frame : (frame) ->
    ff = 
      name : @title
      index : @capture_pos
      data : frame.data

    if socket? and socket.connected
      post_frame_to_server socket.io.uri + "/new_frame", ff


    # console.log frame
    @frames.splice @capture_pos, 0, frame

    if @show_last_frame
      @selected_frame = @capture_pos
      @capture_pos += 1
    else
      @capture_pos += 1
      @selected_frame = @capture_pos

    l = @end - @start + 1
    @end = @get_length() - 1
    @dirty = true

    # frame_update state.get_length() - 1
    # state.fix_values()
    
  set_auto : (i) ->
    @camera_handler.set_auto @camera_handler.settings[i]

  step_setting : () ->
    @setting = wrap @setting + 1, @camera_handler.settings.length
    @dirty = true

  toggle_torch : () ->
    @camera_handler.toggle_torch()

  set_setting : (i, d) ->
    if not @include_live
      @toggle_include_live true
      @set_include_live()
    s = @camera_handler.settings[i]
    v = clamp s.value + d * s.range(), s.min, s.max
    @camera_handler.set s, v

  change_title : (t) ->
    # t = t.sanitize()
    if t.length > 0
      @title = t
      @dirty = true
  change_res : (w, h, remove = false) ->
    if w isnt @camera_handler.width or h isnt @camera_handler.height
      console.log "new resolution", w, h
      if remove
        @remove_all()
      @camera_handler.change_res w, h


  get_title : () -> @title
    # "frames"
  #   get_title = () -> 
  # if ui.title.elt.value.length > 0 then ui.title.elt.value else "state.frames"
  toggle_play : () ->
    @play_pos = 0
    if @frames.length > 0
      @play = not @play
      @dirty = true
  remove_selected : () ->
    i = @selected_frame
    if i >= 0 and i < @get_length() and (i isnt @capture_pos or not s.include_live)
      if i > @capture_pos
        i -= 1
      console.log "removing frame", i
      @frames.splice(i, 1)
      # frame_update current_frame
      # fix_values()
      @fix_values()
      @dirty = true
      # update_frames()
  remove_all : () ->
    if @frames.length > 0
      @frames = []
      @fix_values()
      dirty = true

  step_play_pos : (d) ->
    @play_pos = wrap(@play_pos + d, @get_loop_length())
  reset_play : () ->
    @play_pos = 0
    @time = 0
    @last_frame_time = 0
  update_play_pos : (i) ->
    nt = clamp i, 0, @get_loop_length() - 1
    if @play_pos isnt nt
      @play_pos = nt
      true
    else
      false

  modify_frames : (op, a, b) ->
    if @frames.length is 0
      return

    a = @get_frame_index(a)
    b = @get_frame_index(b)

    console.log op, a, b
    switch op
      when "delete_loop"
        @frames.splice(@start, @get_loop_length())
        @start = 0
        @end = @frames.length - 1
        @selected_frame = clamp @selected_frame, @start, @end
        @capture_pos = clamp @capture_pos, @start, @end
      when "crop"
        s = @selected_frame - @start
        c = @capture_pos - @start
        @frames = @frames.splice(@start, @get_loop_length() - 1)
        @start = 0
        @end = @frames.length
        @selected_frame = s
        @capture_pos = c

      when "swap"
        if a is b then return
        A = @frames[a]
        B = @frames[b]
        @frames[a] = B
        @frames[b] = A
        sel = @selected_frame
        @selected_frame = clamp(@capture_pos, 0, @get_length() - 1)
        @capture_pos = clamp(sel, 0, @get_length() - 1)
      when "move"
        if a is b then return
        A = @frames.splice(a, 1)[0]
        console.log A
        @frames.splice(b, 0, A)
        
        np = 
          if @capture_pos is @get_length() - 1
            @capture_pos - 1
          else 
            @capture_pos

        @selected_frame = clamp(np, 0, @get_length() - 1)

    @dirty = true
    # console.log @frames.length
  scroll_view : (d) ->
    @scroll = Math.floor clamp @scroll + d, get_top(), get_bottom()
    @dirty = true

  snap_scroll : (delta) ->
    y = @scroll
    snap_threshold = menu_height * 1.25
    for m in menu
      # if dir is -1
      if delta > 0
        d = abs(m.y - @scroll)
        if d < snap_threshold
          y = m.y
          break
      else 
        d = abs(m.y - @scroll)
        if d < snap_threshold
          y = m.y + m.h
          break

    @scroll = Math.floor(y)
    @scroll = clamp @scroll, get_top(), get_bottom()
    @dirty = true

  run_animation : (delta) ->
    if @frame_delta isnt 0
      @time = @time + delta

    d = @time - @last_frame_time

    if d >= @frame_delta
      @last_frame_time = @time - (d - @frame_delta)

      nt = @play_pos
      switch @play_mode
        when PLAY_LOOP
          nt = wrap @play_pos + 1, @get_loop_length()

        when PLAY_ONCE
          nt = clamp @play_pos + 1, 0, @get_loop_length() - 1
          if nt is @get_loop_length() - 1
            @toggle_play()

        when PLAY_REVERSE
          nt = wrap @play_pos - 1, @get_loop_length()

        when PLAY_PINGPONG
          if @cycle is 0
            nt = wrap @play_pos + 1, @get_loop_length()
            if nt is @get_loop_length() - 1
              @cycle = 1 - @cycle
          else
            nt = wrap @play_pos - 1, @get_loop_length()
            if nt is 0
              @cycle = 1 - @cycle
            
        # when 4

      if @update_play_pos nt
        @dirty = true

  update : (delta) ->
    # if @scroll_time < 1
    #   @scroll_time += delta * 0.001 * 100
    #   @scroll = lerp(@scroll, @scroll_target, clamp(@scroll_time, 0, 1))
      # if @scroll_time > 1
      #   haptic()

    @camera_handler.update()
    
    if @dirty
      @dirty = false

    if @play
      @run_animation delta
    @aspect = @viewport_width / @viewport_height

  change_fps : (d) ->
    @fps = clamp @fps + d, 1, 60
    @fix_values()
  fix_values : () ->
    @selected_frame = clamp @selected_frame, 0, @get_length() - 1
    @capture_pos = clamp @capture_pos, 0, @frames.length
    @current_frame = clamp @current_frame, 0, @get_length() - 1
    @start = clamp @start, 0, @get_length() - 1
    @end = clamp @end, @start, @get_length() - 1
    @play_pos = clamp @play_pos, 0, @get_loop_length() - 1
    @frame_delta = if @fps is 0 then 0 else (1 / @fps) * 1000
    @play = @frames.length > 0 and @play
    @dirty = true

  reset_loop : () ->
    @end = @get_length() - 1
    @start = 0
    @dirty = true
  get_loop_length : () ->
    @end - @start + 1
  get_length : () ->
    if @include_live
      @frames.length + 1
    else
      @frames.length


  set_include_live : () ->
    @include_live = @next_include_state
    # console.log "live", @include_live

    @selected_frame = clamp @selected_frame, 0, @get_length() - 1

    if @include_live
      # console.log @capture_pos
      # if @capture_pos > @end
        # @capture_pos += 1
      # @selected_frame = @capture_pos
      if @capture_pos isnt @selected_frame
        @capture_pos = @selected_frame + 1
        @selected_frame = @capture_pos
      @end += 1
    else
      @end -= 1

    @fix_values()
    @end = clamp(@end, 0, @get_length() - 1)
    @play_pos = clamp @play_pos, 0, @get_loop_length() - 1
    # if @include_live
    #   @capture_pos = clamp()
    @dirty = true


  toggle_include_live : (l) ->
    if @next_include_state isnt @include_live
      return

    @next_include_state =
      if l?
        l
      else
        not @include_live 

    if @next_include_state
      @camera_handler.start()
    else
      @camera_handler.stop()
      @include_live = false
      # @fix_values()
      @end -= 1
      @end = clamp @end, 0, @get_length() - 1
      @play_pos = clamp @play_pos, 0, @get_loop_length() - 1
      @selected_frame = clamp @selected_frame, 0, @get_length() - 1
    
    @dirty = true
    

  clamp_selection_to_loop : () ->
    @selected_frame = clamp @selected_frame, @start, @end
    @capture_pos = clamp @capture_pos, @start, @end
  change_end : (d) ->
    @end = clamp @end + d, @start, @get_length()
    @selected_frame = @end
    @fix_values()
    @clamp_selection_to_loop()
  change_length : (d) ->
    l = @end - @start + 1
    # if @end + d >= @get_length()
    #   # @start = clamp @start - d, 0, @end
    # else
    @end = clamp @end + d, @start, @get_length() - 1
    @selected_frame = @end
    @fix_values()
    @clamp_selection_to_loop()
  change_start : (d, change_length) ->
    if change_length
      @start = clamp @start + d, 0, @get_length() - 1
      @selected_frame = @start
      @end = clamp @end, @start, @get_length() - 1
    else
      l = @get_loop_length()
      @start = clamp @start + d, 0, @get_length() - l
      @selected_frame = @start
      @end = @start + l - 1

    # @selected_frame = @start
    @fix_values()
    @clamp_selection_to_loop()

  get_frame_index : (i) ->
    if @frames.length is 0
      return 0
    else
      if @include_live
        if i <= @capture_pos then i
        else i - 1
      else
        i
  image_at : (i) ->
    image = 
      if @frames.length is 0
        @camera_handler.image
      else
        i = clamp i, 0, @frames.length - 1
        @frames[i].image
    if image? and image.height is 0 or image.width is 0
      null
    else
      image
  get_frame : (i) ->
    # console.log "frame " + i
    f = 
      if @frames.length is 0
        @camera_handler.image
      else
        if @include_live
          if i < @capture_pos
            @image_at i
          else if i == @capture_pos
            @camera_handler.image
          else
            @image_at i - 1
        else
          @image_at i
    if not f?
      console.log "no frame for ", i

    f
  focus : () ->
    if @on_before_defocus
      @toggle_include_live true
  defocus : () ->
    if @include_live
      @toggle_include_live false
      @on_before_defocus = true
    else
      @on_before_defocus = false
